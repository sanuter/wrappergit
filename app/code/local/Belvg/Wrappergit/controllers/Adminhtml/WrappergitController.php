<?php
/**
 * @package Wrapper Git.
 * @author: A.A.Treitjak
 * @copyright: 2012 - 2013 BelVG.com
 */

class Belvg_Wrappergit_Adminhtml_WrappergitController
    extends Mage_Adminhtml_Controller_Action
{
    /**
     * Prepare header grid.
     *
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('wrappergit/wrappergit')
            ->_addBreadcrumb(
                Mage::helper('wrappergit')->__('Wrapper Git'),
                Mage::helper('wrappergit')->__('Stories')
            )
            ->_addBreadcrumb(
                Mage::helper('wrappergit')->__('Main'),
                Mage::helper('wrappergit')->__('Main')
            );

        $this->_title($this->__('Wrapper Git'))
            ->_title($this->__('Main'));

        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    public function commandAction()
    {
        if($this->getRequest()->isPost()) {
            $command = $this->getRequest()->getParam('command', '');

            if($command) {
                $output = Mage::getSingleton('wrappergit/repository')->run($command);

                Mage::register('wrappergit_output', $output);
            }
        }

        $this->_forward('index');
    }

    public function statusAction()
    {
        $output = Mage::getSingleton('wrappergit/repository')->run('status');

        Mage::register('wrappergit_output', $output);

        $this->_forward('index');
    }

    public function commitAction()
    {
        $output = Mage::getSingleton('wrappergit/repository')->run('commit -am "Fixed changed."');

        Mage::register('wrappergit_output', $output);

        $this->_forward('index');
    }

    public function pushAction()
    {
        $output = Mage::getSingleton('wrappergit/repository')->run('push');

        Mage::register('wrappergit_output', $output);

        $this->_forward('index');
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        $result = TRUE;

        switch ($this->getRequest()->getActionName()) {
            case 'status':
                $result = Mage::getSingleton('admin/session')->isAllowed('wrappergit/actions/status');
                break;

            case 'commit':
                $result = Mage::getSingleton('admin/session')->isAllowed('wrappergit/actions/commit');
                break;

            case 'push':
                $result = Mage::getSingleton('admin/session')->isAllowed('wrappergit/actions/push');
                break;

            case 'command':
                $result = Mage::getSingleton('admin/session')->isAllowed('wrappergit/actions/command');
                break;

            default:
                $result = TRUE;
                break;
        }

        return $result;
    }
}
