<?php
/**
 * @package Wrapper Git.
 * @author: A.A.Treitjak
 * @copyright: 2012 - 2013 BelVG.com
 */

class Belvg_Wrappergit_Model_Repository extends Varien_Object
{
    /**
     * Make git commit.
     *
     * @param string $message
     * @param bool $amend
     *
     * @return array|bool
     */
    public function commit($message = '', $addFiles = FALSE, $amend = FALSE)
    {
        $command = "commit ";

        if ($addFiles) {
            $command .= "-a ";
        }
        if ($amend) {
            $command .= "--amend";
        }
        if ($message) {
            $command .= '-m "' . $message . '"';
        }

        $result = $this->run($command . " --porcelain");

        if (!$result) {
            return FALSE;
        }

        return $this->run($command);
    }

    /**
     * Runs a git command.
     *
     * @param $command
     *
     * @return string
     * @throws Exception
     */
    public function run($command)
    {
        $descriptor = array(
            1 => array('pipe', 'w'),
            2 => array('pipe', 'w'),
        );

        $pipes = array();
        $resource = proc_open($this->getGitPath() . " " . $command, $descriptor, $pipes, $this->getRepoPath());

        $stdout = stream_get_contents($pipes[1]);
        $stderr = stream_get_contents($pipes[2]);

        foreach ($pipes as $pipe) {
            fclose($pipe);
        }

        if (trim(proc_close($resource)) && $stderr) {
            return $stderr;
        }

        return trim($stdout);
    }

    public function setEmailConfig()
    {
        $email = Mage::helper('wrappergit')->getConfigValue('commitemail');

        return $this->run('config user.email "' . $email . '"');
    }

    public function setNameConfig()
    {
        $name = Mage::helper('wrappergit')->getConfigValue('commitname');

        return $this->run('config user.name "' . $name . '"');
    }


    /**
     * @return Varien_Data_Collection
     */
    public function status()
    {
        return $this->run("status --porcelain");
    }

    /**
     * @param bool $force
     *
     * @return string
     */
    public function push($force = FALSE)
    {
        if ($force) {
            $command = "push -f";
        } else {
            $command = "push";
        }

        return $this->run($command);
    }

    /**
     * @return string
     */
    public function fetch()
    {
        return $this->run("fetch");
    }

    public function cron()
    {
        $this->run('commit -am "Fixed changed."');
        $this->run('push');
    }

    /**
     * Init branch.
     */
    protected function _construct()
    {
        $this->setData(
            array(
                'repo_path' => Mage::helper('wrappergit')->getConfigValue('repopath'),
                'git_path' => Mage::helper('wrappergit')->getConfigValue('gitpath'),
                'branches' => array(),
                'commits' => array()
            )
        );
    }
}