<?php
/**
 * @package Wrapper Git.
 * @author: A.A.Treitjak
 * @copyright: 2012 - 2013 BelVG.com
 */

class Belvg_Wrappergit_Model_Branch extends Varien_Object
{
    /**
     * Return commit by hash.
     *
     * @param $hash
     *
     * @return Varien_Object
     */
    public function getCommit($hash)
    {
        return $this->getCommits()->getItemById($hash);
    }

    /**
     * Return all commits in branch.
     *
     * @return Varien_Data_Collection
     */
    public function getCommits()
    {
        $command = 'log --pretty=format:"%H" ' . $this->getName();
        $commits = new Varien_Data_Collection();

        foreach (explode("\n", $this->getRepository()->run($command)) as $hash) {
            $hash = trim($hash);

            if (!$hash) {
                continue;
            }

            $commit = Mage::getModel('wrappergit/commit');
            $commit->setHash($hash);

            $commits->addItem($commit);
        }

        return $commits;
    }

    /**
     * Return last commit.
     */
    public function getLastCommit()
    {
        $this->getCommits()->getLastItem();
    }

    /**
     * Init branch.
     */
    protected function _construct()
    {
        $this->setData(
            array(
                'name' => 'master',
                'repository' => Mage::getSingleton('wrappergit/repository')
            )
        );
    }
}