<?php
/**
 * @package Wrapper Git.
 * @author: A.A.Treitjak
 * @copyright: 2012 - 2013 BelVG.com
 */

class Belvg_Wrappergit_Model_Commit extends Varien_Object
{
    /**
     * Return list files of commit.
     *
     * @return mixed
     */
    public function getFiles()
    {
        if (!$this->getData('files')) {
            $command = 'show --pretty="format:" --name-only ' . $this->getHash();
            $files = new Varien_Data_Collection();

            foreach (explode("\n", $this->getRepository()->run($command)) as $line) {
                $file = new Varien_Object(array(
                    'name' => trim($line)
                ));

                $files->addItem($file);
            }

            $this->setData('files', $files);
        }

        return $this->getData('files');
    }

    /**
     * Init commit.
     */
    protected function _construct($hash)
    {
        $this->setData(
            array(
                'hash' => '',
                'repository' => Mage::getSingleton('wrappergit/repository'),
                'author_name' => Mage::helper('wrappergit')->getConfigValue('commitname'),
                'author_email' => Mage::helper('wrappergit')->getConfigValue('commitemail'),
                'time' => time(),
                'subject' => '',
                'message' => '',
            )
        );

        $delimiter = "|||---|||---|||";
        $command = 'show --pretty=format:"%an' . $delimiter . '%ae' . $delimiter . '%cd' . $delimiter . '%s' . $delimiter . '%B' . $delimiter . '%N" ' . $this->hash;

        $response = $this->getRepository()->run($command);

        $parts = explode($delimiter, $response);

        $this->setData('author_name', array_shift($parts));
        $this->setData('author_email', array_shift($parts));
        $this->setData('time', array_shift($parts));
        $this->setData('subject', array_shift($parts));
        $this->setData('message', array_shift($parts));
    }
}