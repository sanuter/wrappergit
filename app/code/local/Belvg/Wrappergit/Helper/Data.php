<?php
/**
 * @package Wrapper Git.
 * @author: A.A.Treitjak
 * @copyright: 2012 - 2013 BelVG.com
 */

class Belvg_Wrappergit_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Key module in config.
     */
    const XML_CONFIG_PATH = 'wrappergit/settings/';

    /**
     * Check enabled module.
     *
     * @param string $store
     *
     * @return bool
     */
    public function isEnabled($store = '')
    {
        return (bool) $this->_getConfigValue('enabled', $store);
    }

    /**
     * Config value.
     *
     * @param $key
     * @param $store
     *
     * @return mixed
     */
    protected function _getConfigValue($key, $store = '')
    {
        return Mage::getStoreConfig(self::XML_CONFIG_PATH . $key, $store);
    }

    /**
     * Return value from config.
     *
     * @param $key
     * @param string $store
     *
     * @return mixed
     */
    public function getConfigValue($key, $store = '')
    {
        return $this->_getConfigValue($key, $store);
    }
}