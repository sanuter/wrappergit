<?php
/**
 * @package Wrapper Git.
 * @author: A.A.Treitjak
 * @copyright: 2012 - 2013 BelVG.com
 */

class Belvg_Wrappergit_Block_Adminhtml_Tools extends Mage_Adminhtml_Block_Widget_Form
{
    public function getOutput()
    {
        $output = Mage::registry('wrappergit_output');

        return nl2br($output);
    }
}